(* ::Package:: *)

<<MathematicaM2`


PolynomialFactor[poly1_,varlist_]:=Module[{poly=poly1,vlist=varlist,list,i,result={}},
list=FactorList[poly];
For[i=1,i<=Length[list],i++,
If[Intersection[Variables[list[[i,1]]],vlist]!={},
AppendTo[result,list[[i]]];
];
];
Return[result];
];
reduceFactor[poly1_,varlist_]:=Module[{poly=poly1,vlist=varlist,n,list},
list=PolynomialFactor[poly,vlist];
n=Length[list];

Return[Product[list[[i,1]]^list[[i,2]],{i,1,n}]];

];


PolynomialOffset[target1_,ref1_,varlist_]:=
Module[{target=target1,ref=ref1,vlist=varlist,i,j,result,list1,list2,powerlist},
list1=PolynomialFactor[target,vlist];
list2=PolynomialFactor[ref,vlist];
powerlist=Table[0,{i,1,Length[list2]}];

For[i=1,i<=Length[list2],i++,
For[j=1,j<=Length[list1],j++,
If[list2[[i,1]]==list1[[j,1]]&&list1[[j,2]]-list2[[i,2]]>0,
powerlist[[i]]=(list1[[j,2]]-list2[[i,2]]);
Break[];];
];
];

Return[Product[list2[[i,1]]^powerlist[[i]],{i,1,Length[list2]}]];

];


Project1[exp1_]:=Module[{exp=exp1,result},
result=exp/(w0^3)/.{z1->w1/w0,z2->1/w0};
Return[result//Simplify];
];
Project2[exp1_]:=Module[{exp=exp1,result},
result=exp/(-w0^3)/.{z1->1/w0,z2->w2/w0};
Return[result//Simplify];
];


ResidueLocus[list1_,vlist1_]:=Module[{fList=list1,vlist=vlist1,len,fProd,residueLocuslist,F1,F2,ss,k,i,j},

len=fList//Length;
fProd=Product[fList[[i]],{i,1,len}];
residueLocuslist={};
For[i=1,i<=len,i++,
For[j=i+1,j<=len,j++,
F1=fList[[i]];
F2=fList[[j]];
ss=Solve[{F1,F2}=={0,0},vlist];
For[k=1,k<=Length[ss],k++,
AppendTo[residueLocuslist,{vlist/.ss[[k]],{F1,F2}}];
];
];
];
Return[residueLocuslist//Sort];
];


ShiftList[list_]:=Append[Table[list[[i]],{i,2,Length[list]}],list[[1]]];


Transformation[fList1_,varlist1_]:=Module[{fList=fList1,varlist=varlist1,n,i,j,var,result,Matrix,Q,M,coefficientList},
n=Length[fList];
var=varlist;

coefficientList=Complement[Variables[fList],varlist];

result={};
Matrix={};

For[i=1,i<=n,i++,
var=ShiftList[var];

{Q,M}=M2GroebnerBasis[fList,var,GeneratingMatrix->True,MonomialOrder->Lexicographic,Coefficients->coefficientList];
AppendTo[result,Q[[1]]];
AppendTo[Matrix,Transpose[M][[1]]];
];
Return[{result,Matrix}];
];


UnivariateResidue[H1_,fList1_,varlist1_,locus1_]:=Module[{H=H1,fList=fList1,varlist=varlist1,locus=locus1,result,i},
result=H/Product[fList[[i]],{i,1,Length[fList]}];
For[i=1,i<=Length[varlist],i++,
result=SeriesCoefficient[result,{varlist[[i]],locus[[i]],-1}];
];
Return[result];
];


RegularResidue[H1_,fList1_,varlist1_,locus1_]:=Module[{H=H1,fList=fList1,varlist=varlist1,locus=locus1,result,J,i,numeric},
numeric=Table[varlist[[i]]->locus[[i]],{i,1,Length[varlist]}];
If[Simplify[(fList/.numeric)==Table[0,{i,1,Length[varlist]}]],



J=Det[D[fList,{varlist}]]/.numeric;

J=Simplify[J];
If[J==0,Return["SinguluarFlag"]];

Return[H/J/.Table[varlist[[i]]->locus[[i]],{i,1,Length[varlist]}]];
];
Return[0];
];


MultiResidue[H1_,fList1_,varlist1_,locus1_]:=
Module[{H=H1,fList=fList1,varlist=varlist1,locus=locus1,result,qList,M},
result=RegularResidue[H,fList,varlist,locus];
If[result=="SinguluarFlag",
{qList,M}=Transformation[fList,varlist];
result=UnivariateResidue[H*Det[M]//Simplify,qList,varlist,locus];
];
Return[result];
];



residue[exp1_,fList1_,varlist1_,locus1_]:=Module[{exp=exp1,fList=fList1,varlist=varlist1,locus=locus1,result,num,den,n,offsetList,h},

n=Length[varlist];
exp=Together[exp];
num=Numerator[exp];
den=Denominator[exp];

offsetList=Table[PolynomialOffset[den,fList[[i]],varlist],{i,1,n}];

fList=Table[fList[[i]]*offsetList[[i]],{i,1,n}];


h=num/(den)*Product[fList[[i]],{i,1,n}]//Simplify;


result=MultiResidue[h,fList,varlist,locus];

Return[result//Simplify];
];

